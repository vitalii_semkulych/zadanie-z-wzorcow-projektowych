/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bron;

/**
 *
 * @author Admin
 */
public abstract class Karabiny implements InterfaceKarabinow {

    protected String nazwa;
    protected String color;
    protected int cena;
    protected String panstwo;
    protected int kaliber;
    protected int dlugoscBroni;
    protected int dlugoscLufy;
    protected int masa;
    protected int pojemnoscMagazynu;

    public Karabiny(String nazwa, String color, int cena, String panstwo, int kaliber, int dlugoscBroni, int dlugoscLufy, int masa, int pojemnoscMagazynu) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.color = color;
        this.panstwo = panstwo;
        this.kaliber = kaliber;
        this.dlugoscBroni = dlugoscBroni;
        this.dlugoscLufy = dlugoscLufy;
        this.masa = masa;
        this.pojemnoscMagazynu = pojemnoscMagazynu;
    }

    public Karabiny() {
        nazwa = "brak";
        color = "brak";
        cena = 0;
        panstwo = "brak";
        kaliber = 0;
        dlugoscBroni = 0;
        dlugoscLufy = 0;
        masa = 0;
        pojemnoscMagazynu = 0;
    }

}
