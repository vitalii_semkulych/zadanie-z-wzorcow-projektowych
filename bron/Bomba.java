/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bron;

/**
 *
 * @author Admin
 */
public abstract class Bomba implements InterfaceBomb {

    protected String nazwa;
    protected String color;
    protected int cena;
    protected String panstwo;
    protected int masa;

    public Bomba(String nazwa, String color, int cena, String panstwo, int kaliber, int dlugoscBroni, int dlugoscLufy, int masa, int pojemnoscMagazynu) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.color = color;
        this.panstwo = panstwo;
        this.masa = masa;

    }

    public Bomba() {
        nazwa = "brak";
        color = "brak";
        cena = 0;
        panstwo = "brak";
        masa = 0;

    }

}
